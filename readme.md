## Global Optimization Using Matchables: G2O Plugin
Simple and easy to embed plugin to perform optimization with matchable.

### How to get the plugin working
The plugin can be easily embedded in your g2o installation following these instructions.

#### Prerequisites
1. Cholmod and CSparse libraries
```bash
sudo apt install libsuitesparse-dev
```
2. Fresh g2o installation. Follow the instructions [here](https://github.com/RainerKuemmerle/g2o)

3. G2O Chordal Plugin. Follow the instructions [here](https://gitlab.com/srrg-software/srrg_g2o_chordal_plugin)

#### Installation
1. Link the folder `plugin_matchables` in your
local folder `<g2o_root>/g2o/`

2. Add the following lines to the file `<g2o_root>/g2o/CMakeLists.txt` (at the end of the file)
```cmake
add_subdirectory(plugin_matchables)
```

3. Clean build g2o:
```bash
cd <g2o_root>/build
make clean
rm -rf *
cmake -DBUILD_WITH_MARCH_NATIVE=ON ..
make
```

#### How to test the installation
1. Generate a graph using our simulator
```bash
cd <g2o-bin-dir>
./g2o_matchable_simulator3d -h
```
2. Add noise to the graph
```bash
./g2o_matchable_noise_adder -h
```
3. Run g2o

### Authors
* [Irvin Aloise](https://istinj.github.io/)

### Related publications
Please cite our most recent article if you use our software:
```
@article{aloise2019systematic,
  title={Systematic Handling of Heterogeneous Geometric Primitives in Graph-SLAM Optimization},
  author={Aloise, Irvin and Della Corte, Bartolomeo and Nardi, Federico and Grisetti, Giorgio},
  journal={IEEE Robotics and Automation Letters},
  volume={4},
  number={3},
  pages={2738--2745},
  year={2019},
  publisher={IEEE}
  doi={10.1109/LRA.2019.2918054}
}
```

### Something is not working?
Open an [issue](https://gitlab.com/srrg-software/srrg_g2o_matchable_plugin/issues/new) or contact one of the authors.

### License
BSD-4 License
