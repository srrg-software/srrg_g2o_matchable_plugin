#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// ia include g2o core stuff
#include <g2o/core/factory.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/stuff/color_macros.h>
#include <g2o/stuff/command_args.h>
#include <g2o/stuff/sampler.h>

// ia include types
#include <g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h>
#include <g2o/types/slam3d/isometry3d_mappings.h>

// ia matchables
#include "g2o/plugin_matchables/matchable_simulator/matchable_world.h"
#include "g2o/plugin_matchables/matchable_simulator/mixed_gaussian_noise_sampler.h"
#include "g2o/plugin_matchables/matchable_types/types_matchables.h"

using namespace g2o;
using namespace matchables;

using MatchableMixedNoiseSampler = MixedGaussianNoiseSampler<MatchableGaussianNoiseSampler>;
using Pose3dMixedNoiseSampler    = MixedGaussianNoiseSampler<Pose3dGaussianNoiseSampler>;

void addMatchableNoise(EdgeSE3Matchable* edge_, MatchableMixedNoiseSampler& sampler_);

void addPoseNoise(EdgeSE3ChordalErrorA* edge_, Pose3dMixedNoiseSampler& sampler_);

int main(int argc, char** argv) {
  std::cerr << "graph must be in a OPTMIMUM STATE" << std::endl << std::endl;

  // ia for the factory
  VertexSE3EulerPert v_se3;
  VertexMatchable v_match;
  EdgeSE3ChordalErrorA e_se3;
  EdgeSE3Matchable e_match;
  std::string e_matchable_tag = Factory::instance()->tag(&e_match);
  std::string e_se3_tag       = Factory::instance()->tag(&e_se3);

  // ia input params
  std::string input_filename;
  std::string output_filename;

  std::vector<number_t> point_noise_low;   // ia noise on the matchable point  - low
  std::vector<number_t> normal_noise_low;  // ia noise on the matchable normal - low
  std::vector<number_t> point_noise_mid;   // ia noise on the matchable point  - mid
  std::vector<number_t> normal_noise_mid;  // ia noise on the matchable normal - mid
  std::vector<number_t> point_noise_high;  // ia noise on the matchable point  - high
  std::vector<number_t> normal_noise_high; // ia noise on the matchable normal - high

  std::vector<number_t> translational_noise_low;  // ia noise on the robot position
  std::vector<number_t> rotational_noise_low;     // ia noise on the robot rotation
  std::vector<number_t> translational_noise_mid;  // ia noise on the robot position
  std::vector<number_t> rotational_noise_mid;     // ia noise on the robot rotation
  std::vector<number_t> translational_noise_high; // ia noise on the robot position
  std::vector<number_t> rotational_noise_high;    // ia noise on the robot rotation

  g2o::CommandArgs arg;
  arg.param("o", output_filename, "", "output final version of the graph");
  arg.param("pnLow",
            point_noise_low,
            std::vector<number_t>(3, 0.005),
            "translational noise as <nx,ny,nz> - LOW");
  arg.param("nnLow",
            normal_noise_low,
            std::vector<number_t>(2, 0.001),
            "matchable normal noise as <nx,ny> - LOW");
  arg.param("pnMid",
            point_noise_mid,
            std::vector<number_t>(3, 0.05),
            "translational noise as <nx,ny,nz> - MID");
  arg.param("nnMid",
            normal_noise_mid,
            std::vector<number_t>(2, 0.01),
            "matchable normal noise as <nx,ny> - MID");
  arg.param("pnHigh",
            point_noise_high,
            std::vector<number_t>(3, 0.5),
            "translational noise as <nx,ny,nz> - HIGH");
  arg.param("nnHigh",
            normal_noise_high,
            std::vector<number_t>(2, 0.1),
            "matchable normal noise as <nx,ny> - HIGH");

  arg.param("tnLow",
            translational_noise_low,
            std::vector<number_t>(3, 0.01),
            "odometry noise for translation as <nx,ny,nz> - LOW");
  arg.param("rnLow",
            rotational_noise_low,
            std::vector<number_t>(3, 0.001),
            "odometry noise for rotation as <nr,np,ny> - LOW");
  arg.param("tnMid",
            translational_noise_mid,
            std::vector<number_t>(3, 0.1),
            "odometry noise for translation as <nx,ny,nz> - MID");
  arg.param("rnMid",
            rotational_noise_mid,
            std::vector<number_t>(3, 0.01),
            "odometry noise for rotation as <nr,np,ny> - MID");
  arg.param("tnHigh",
            translational_noise_high,
            std::vector<number_t>(3, 1.0),
            "odometry noise for translation as <nx,ny,nz> - HIGH");
  arg.param("rnHigh",
            rotational_noise_high,
            std::vector<number_t>(3, 0.1),
            "odometry noise for rotation as <nr,np,ny> - HIGH");

  arg.paramLeftOver("graph-input", input_filename, "", "input file (already at the optimum)", true);
  arg.parseArgs(argc, argv);

  if (!input_filename.size()) {
    throw std::runtime_error("please set a valid imput graph");
  }

  // ia setup matchable noise
  MatchableMixedNoiseSampler random_m_noise_sampler(0.33, 0.33);
  random_m_noise_sampler.setNoiseStats(point_noise_low,
                                       normal_noise_low,
                                       point_noise_mid,
                                       normal_noise_mid,
                                       point_noise_high,
                                       normal_noise_high);

  Pose3dMixedNoiseSampler random_p_noise_sampler(0.33, 0.33);
  random_p_noise_sampler.setNoiseStats(translational_noise_low,
                                       rotational_noise_low,
                                       translational_noise_mid,
                                       rotational_noise_mid,
                                       translational_noise_high,
                                       rotational_noise_high);

  // ia load the graph
  g2o::SparseOptimizer optimizer;
  std::cerr << "opening file : " << input_filename << std::endl;
  std::ifstream ifs(input_filename.c_str());
  optimizer.load(ifs);

  g2o::HyperGraph::EdgeSet& edges = optimizer.edges();
  const size_t num_vertices       = optimizer.vertices().size();
  const size_t num_edges          = optimizer.edges().size();
  std::cerr << "read " << num_vertices << " vertices and " << num_edges << " edges" << std::endl
            << std::endl;

  size_t cnt = 0;
  std::cerr << "applying noise to the edges" << std::endl;
  for (g2o::HyperGraph::Edge* e : edges) {
    float percentage = std::ceil((float) cnt++ / (float) num_edges * 100);
    if ((int) percentage % 5 == 0)
      std::cerr << "\rcompleted " << percentage << "%" << std::flush;

    if (e_matchable_tag == Factory::instance()->tag(e)) {
      EdgeSE3Matchable* e_matchable = dynamic_cast<EdgeSE3Matchable*>(e);
      addMatchableNoise(e_matchable, random_m_noise_sampler);
      continue;
    }

    if (e_se3_tag == Factory::instance()->tag(e)) {
      EdgeSE3ChordalErrorA* e_se3_chordal = dynamic_cast<EdgeSE3ChordalErrorA*>(e);
      addPoseNoise(e_se3_chordal, random_p_noise_sampler);
      continue;
    }

    std::cerr << "unrecognized edge type: " << Factory::instance()->tag(e) << std::endl;
  }

  std::cerr << std::endl;
  std::cerr << "matchable noise sample stats:\n";
  random_m_noise_sampler.printStats();
  std::cerr << "odometry noise sample stats:\n";
  random_p_noise_sampler.printStats();

  std::cerr << "saving noisy graph in : " << output_filename << std::endl;
  optimizer.save(output_filename.c_str());

  return 0;
}

void addMatchableNoise(EdgeSE3Matchable* edge_, MatchableMixedNoiseSampler& sampler_) {
  // ia here gt is just the measurement since there is no noise encoded here yet
  const Matchable Z_gt = edge_->measurement();

  Vector5 noise_sample = Vector5::Zero();
  Matrix7 noise_omega  = Matrix7::Zero();
  sampler_.randomSample(noise_sample, noise_omega);
  //  std::cerr << "addMatchableNoise|sampled: " << noise_sample.transpose() << std::endl;
  //  std::cerr << noise_omega << std::endl;

  // ia TODO check this information matrix if it is good or not
  Matchable noisy_matchable = Z_gt.applyMinimalPert(noise_sample);
  Matrix7 noisy_information = edge_->information();

  noisy_information *= noise_omega;

  edge_->setMeasurement(noisy_matchable);
  edge_->setInformation(noisy_information);

  // std::cerr << "new information\n" << noisy_information << std::endl;
  // std::cin.get();
}

void addPoseNoise(EdgeSE3ChordalErrorA* edge_, Pose3dMixedNoiseSampler& sampler_) {
  // ia here gt is just the measurement since there is no noise encoded here yet
  const Isometry3& z_gt = edge_->measurement();

  Vector6 noise_sample = Vector6::Zero();
  Matrix12 noise_omega = Matrix12::Zero();
  sampler_.randomSample(noise_sample, noise_omega);
  //  std::cerr << "addPoseNoise|sampled: " << noise_sample.transpose() << std::endl;
  //  std::cerr << noise_omega << std::endl;

  Isometry3 pose_noise = Isometry3::Identity();
  pose_noise           = internal::fromVectorET(noise_sample);
  Isometry3 noisy_meas = Isometry3::Identity();
  noisy_meas           = z_gt * pose_noise;

  // ia the information matrix is a mess,
  // ia it should be sampled in the chordal distance error space
  edge_->setMeasurement(noisy_meas);
  edge_->setInformation(noise_omega);
  // std::cin.get();
}
