#include "gaussian_noise_sampler.h"

namespace g2o {
  namespace matchables {

    MatchableGaussianNoiseSampler::MatchableGaussianNoiseSampler(const SigmaTType& sigma_t_,
                                                                 const SigmaRType& sigma_r_):
                                                                   GaussianNoiseSamplerTRMatchable(sigma_t_, sigma_r_){
      _omega = Matrix7::Zero();
      _omega.block<3,3>(0,0) =  _t_sampler->omega;
      _omega.block<2,2>(4,4) =  _r_sampler->omega;
    }

    MatchableGaussianNoiseSampler::~MatchableGaussianNoiseSampler() {}


    void MatchableGaussianNoiseSampler::reset(const SigmaTType& sigma_t_,
                                              const SigmaRType& sigma_r_) {
      GaussianNoiseSamplerTRMatchable::reset(sigma_t_, sigma_r_);

      _omega = Matrix7::Zero();
      _omega.block<3,3>(0,0) =  _t_sampler->omega;
      _omega.block<2,2>(4,4) =  _r_sampler->omega;
    }


    // ----------------------------------------------------------------------------------//
    // ----------------------------------------------------------------------------------//
    // ----------------------------------------------------------------------------------//
    Pose3dGaussianNoiseSampler::Pose3dGaussianNoiseSampler(const SigmaTType& sigma_t_,
                                                           const SigmaRType& sigma_r_):
                                                               GaussianNoiseSamplerTRPose3d(sigma_t_, sigma_r_){
      _omega = Matrix12::Identity();
      _omega.block<9,9>(0,0) *= 1000;
      _omega.block<3,3>(9,9) = _t_sampler->omega;
    }

    Pose3dGaussianNoiseSampler::~Pose3dGaussianNoiseSampler(){}


    void Pose3dGaussianNoiseSampler::reset(const SigmaTType& sigma_t_,
                                           const SigmaRType& sigma_r_) {
      GaussianNoiseSamplerTRPose3d::reset(sigma_t_, sigma_r_);

      _omega = Matrix12::Identity();
      _omega.block<9,9>(0,0) *= 1000;
      _omega.block<3,3>(9,9) = _t_sampler->omega;
    }

  } /* namespace matchables */
} /* namespace g2o */
