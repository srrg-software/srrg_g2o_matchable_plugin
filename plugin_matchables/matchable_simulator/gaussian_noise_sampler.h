#pragma once
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// ia include g2o core stuff
#include <g2o/stuff/sampler.h>

// ia some useful math
#include <g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h>

// ia include the matchable typedefs
#include "g2o/plugin_matchables/matchable_types/matchable.h"

namespace g2o {
  namespace matchables {

    //! @brief basic AWGN noise sampler.
    //!        you must specify the dimension of the noise space
    template <size_t Dim_>
    struct GaussianNoiseSampler_ {
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
      using NoiseMeanType         = VectorN<Dim_>;
      using NoiseCovarianceType   = MatrixN<Dim_>;
      static constexpr size_t Dim = Dim_;

      //! @brief actual noise sampler
      GaussianSampler<NoiseMeanType, NoiseCovarianceType> sampler;

      //! @brief actual noise stats
      const NoiseCovarianceType sigma;
      const NoiseCovarianceType omega;

      //! @brief ctor
      GaussianNoiseSampler_(const NoiseCovarianceType& sigma_) :
        sigma(sigma_),
        omega(sigma_.inverse()) {
        sampler.setDistribution(sigma);
      }

      //! @brief sample from the noise distribution
      inline NoiseMeanType sample() {
        return sampler.generateSample();
      }
    };

    //! @brief useful typedefs
    using GaussianNoiseSampler3 = GaussianNoiseSampler_<3>;
    using GaussianNoiseSampler2 = GaussianNoiseSampler_<2>;

    //! @brief generic noise adder for 2 components
    //!        it samples differently for translation and rotation
    //!        template arguments:
    //!        DimT_: dimension of the samples for the translation
    //!        DimR_: dimension of the samples for the orientation
    //!        DimPert_: dimension of the total perturbation (usually DimT+DimR)
    //!        DimOmega_: dimension of the total omega
    template <size_t DimT_, size_t DimR_, size_t DimPert_, size_t DimOmega_>
    class GaussianNoiseSamplerTR {
    public:
      //! @brief useful typedefs
      static constexpr size_t DimT     = DimT_;
      static constexpr size_t DimR     = DimR_;
      static constexpr size_t DimPert  = DimPert_;
      static constexpr size_t DimOmega = DimOmega_;
      using SigmaTType                 = MatrixN<DimT>;
      using SigmaRType                 = MatrixN<DimR>;
      using PertType                   = VectorN<DimPert>;
      using OmegaType                  = MatrixN<DimOmega>;
      using GaussianNoiseSamplerT      = GaussianNoiseSampler_<DimT>;
      using GaussianNoiseSamplerR      = GaussianNoiseSampler_<DimR>;

      //! @brief ctor/dtor
      GaussianNoiseSamplerTR(const SigmaTType& sigma_t_, const SigmaRType& sigma_r_) {
        _t_sampler = new GaussianNoiseSamplerT(sigma_t_);
        _r_sampler = new GaussianNoiseSamplerR(sigma_r_);

        _omega = OmegaType::Zero();

        _num_samples_generated = 0;
      }

      virtual ~GaussianNoiseSamplerTR() {
        if (_t_sampler)
          delete _t_sampler;
        if (_r_sampler)
          delete _r_sampler;

        _t_sampler             = 0;
        _r_sampler             = 0;
        _num_samples_generated = 0;
      }

      //! @brief reset samplers and construct new one with
      //!        new statistics. resets sampler counter
      virtual void reset(const SigmaTType& sigma_t_, const SigmaRType& sigma_r_) {
        if (_t_sampler)
          delete _t_sampler;
        if (_r_sampler)
          delete _r_sampler;

        _t_sampler = 0;
        _r_sampler = 0;
        _t_sampler = new GaussianNoiseSamplerT(sigma_t_);
        _r_sampler = new GaussianNoiseSamplerR(sigma_r_);

        _omega = OmegaType::Zero();

        _num_samples_generated = 0;
      }

      //! @brief get a new sample from noise distribution
      //!        it can be directly directly added to a matchable
      PertType noiseSample() {
        PertType noise_pertubation   = PertType::Zero();
        noise_pertubation.head(DimT) = _t_sampler->sample();
        noise_pertubation.tail(DimR) = _r_sampler->sample();

        ++_num_samples_generated;

        return noise_pertubation;
      }

      //! @brief get noise omega as Sigma^-1
      inline const OmegaType& noiseOmega() const {
        return _omega;
      }

      //! @brief inline accessor
      inline const size_t& numSamplesGenerated() const {
        return _num_samples_generated;
      }

    protected:
      //! @brief gaussian samplers for two components
      GaussianNoiseSamplerT* _t_sampler = 0;
      GaussianNoiseSamplerR* _r_sampler = 0;

      //! @brief total omega, stays the same until reset, so
      //!        no need to compute it every time
      OmegaType _omega;

      //! @brief counter
      size_t _num_samples_generated;

    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    };

    using GaussianNoiseSamplerTRMatchable = GaussianNoiseSamplerTR<3, 2, 5, 7>;
    using GaussianNoiseSamplerTRPose3d    = GaussianNoiseSamplerTR<3, 3, 6, 12>;

    //! @brief specialization for matchable noise
    class MatchableGaussianNoiseSampler : public GaussianNoiseSamplerTRMatchable {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
      // ia this is a shit, just for now

      //! @brief ctor/dtor
      MatchableGaussianNoiseSampler(const SigmaTType& sigma_t_, const SigmaRType& sigma_r_);
      ~MatchableGaussianNoiseSampler();

      //! @brief reset samplers and construct new one with
      //!        new statistics. resets sampler counter
      virtual void reset(const SigmaTType& sigma_t_, const SigmaRType& sigma_r_);
    };

    //! @brief specialization for pose3d noise
    class Pose3dGaussianNoiseSampler : public GaussianNoiseSamplerTRPose3d {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

      //! @brief ctor/dtor
      Pose3dGaussianNoiseSampler(const SigmaTType& sigma_t_, const SigmaRType& sigma_r_);
      ~Pose3dGaussianNoiseSampler();

      //! @brief reset samplers and construct new one with
      //!        new statistics. resets sampler counter
      void reset(const SigmaTType& sigma_t_, const SigmaRType& sigma_r_);
    };

  } /* namespace matchables */
} /* namespace g2o */
