#pragma once
#include "gaussian_noise_sampler.h"
#include "matchable_world.h" //ia this is a shit

namespace g2o {
  namespace matchables {

    template <class GaussianNoiseSamplerType_>
    class MixedGaussianNoiseSampler;

    // ia this is a shit, just for now
    template <typename GaussianNoiseSamplerType_>
    class MixedGaussianNoiseSampler {
    public:
      using SamplerType                = GaussianNoiseSamplerType_;
      static constexpr size_t DimT     = GaussianNoiseSamplerType_::DimT;
      static constexpr size_t DimR     = GaussianNoiseSamplerType_::DimR;
      static constexpr size_t DimPert  = GaussianNoiseSamplerType_::DimPert;
      static constexpr size_t DimOmega = GaussianNoiseSamplerType_::DimOmega;
      using SigmaTType                 = MatrixN<DimT>;
      using SigmaRType                 = MatrixN<DimR>;
      using PertType                   = VectorN<DimPert>;
      using OmegaType                  = MatrixN<DimOmega>;

      MixedGaussianNoiseSampler(const float& low_t_, const float& high_t_) :
        _low_thresh(low_t_),
        _mid_thresh(1.0f - high_t_) {
        _setup = false;
      }
      ~MixedGaussianNoiseSampler() {
        if (_low_noise_sampler)
          delete _low_noise_sampler;
        if (_mid_noise_sampler)
          delete _mid_noise_sampler;
        if (_high_noise_sampler)
          delete _high_noise_sampler;
        _setup = false;
      }

      //! @brief set noise stats from vectors (command line friendly)
      void setNoiseStats(const std::vector<number_t>& p_l_vec_,
                         const std::vector<number_t>& n_l_vec_,
                         const std::vector<number_t>& p_m_vec_,
                         const std::vector<number_t>& n_m_vec_,
                         const std::vector<number_t>& p_h_vec_,
                         const std::vector<number_t>& n_h_vec_) {
        if ((p_l_vec_.size() != DimT && p_m_vec_.size() != DimT && p_h_vec_.size() != DimT) ||
            (n_l_vec_.size() != DimR && n_m_vec_.size() != DimR && n_h_vec_.size() != DimR)) {
          throw std::runtime_error("MixedGaussianNoiseSampler::setNoiseStats|invalid stats size");
        }

        // ia points
        for (size_t u = 0; u < DimT; ++u) {
          _sigma_low_t(u, u)  = std::pow(p_l_vec_[u], 2);
          _sigma_mid_t(u, u)  = std::pow(p_m_vec_[u], 2);
          _sigma_high_t(u, u) = std::pow(p_h_vec_[u], 2);
        }

        // ia normals
        for (size_t u = 0; u < DimR; ++u) {
          _sigma_low_r(u, u)  = std::pow(n_l_vec_[u], 2);
          _sigma_mid_r(u, u)  = std::pow(n_m_vec_[u], 2);
          _sigma_high_r(u, u) = std::pow(n_h_vec_[u], 2);
        }

        // ia setup noise figures
        _low_noise_sampler  = new SamplerType(_sigma_low_t, _sigma_low_r);
        _mid_noise_sampler  = new SamplerType(_sigma_mid_t, _sigma_mid_r);
        _high_noise_sampler = new SamplerType(_sigma_high_t, _sigma_high_r);

        // ia setup noise selector - no random seed
        _noise_selector.setMax(1.0);
        _noise_selector.setMin(0.0);
        _noise_selector.setup();

        std::cerr << "MixedGaussianNoiseSampler::setNoiseStats|noise figure setup\n";
        std::cerr << "\t low noise | trans ["
                  << CL_YELLOW(_sigma_low_t.diagonal().matrix().transpose()) << "] | rot ["
                  << CL_YELLOW(_sigma_low_r.diagonal().matrix().transpose()) << "]" << std::endl;
        std::cerr << "\t mid noise | trans ["
                  << CL_YELLOW(_sigma_mid_t.diagonal().matrix().transpose()) << "] | rot ["
                  << CL_YELLOW(_sigma_mid_r.diagonal().matrix().transpose()) << "]" << std::endl;
        std::cerr << "\t high noise| trans ["
                  << CL_YELLOW(_sigma_high_t.diagonal().matrix().transpose()) << "] | rot ["
                  << CL_YELLOW(_sigma_high_r.diagonal().matrix().transpose()) << "]" << std::endl;
        std::cerr << "MixedGaussianNoiseSampler::setNoiseStats|setup complete\n";

        _setup = true;
      }

      void randomSample(PertType& noise_perturbation_, OmegaType& noise_omega_) {
        if (!_setup)
          throw std::runtime_error(
            "MixedMatchableNoiseSampler::sampleRandomNoise|unknown noise figures");

        noise_perturbation_.setZero();
        noise_omega_.setZero();

        // ia select what noise figure to use
        const number_t n_type = _noise_selector.sample();

        if (n_type < _low_thresh) {
          // ia low noise
          noise_perturbation_ = _low_noise_sampler->noiseSample();
          noise_omega_        = _low_noise_sampler->noiseOmega();
        } else if (_low_thresh < n_type && n_type < _mid_thresh) {
          // ia mid noise
          noise_perturbation_ = _mid_noise_sampler->noiseSample();
          noise_omega_        = _mid_noise_sampler->noiseOmega();
        } else {
          // ia high noise
          noise_perturbation_ = _high_noise_sampler->noiseSample();
          noise_omega_        = _high_noise_sampler->noiseOmega();
        }

        return;
      }

      //! @brief print stats
      void printStats() {
        if (!_setup)
          throw std::runtime_error("MixedGaussianNoiseSampler::printStats|not setup");

        std::printf(
          "MixedGaussianNoiseSampler::printStats|low [%05lu] - mid [%05lu] - high [%05lu]\n",
          _low_noise_sampler->numSamplesGenerated(),
          _mid_noise_sampler->numSamplesGenerated(),
          _high_noise_sampler->numSamplesGenerated());
      }

    protected:
      //! @brief select from which noise level we will sample this time
      //!        no random seed. always the same
      UniformSampler _noise_selector;

      //! @brief noise samplers
      SamplerType* _low_noise_sampler  = 0;
      SamplerType* _mid_noise_sampler  = 0;
      SamplerType* _high_noise_sampler = 0;

      //! @brief each of them requires two matrices to specify noise stats
      //!        for the rotation and the translation part
      SigmaTType _sigma_low_t = SigmaTType::Zero();
      SigmaRType _sigma_low_r = SigmaRType::Zero();

      SigmaTType _sigma_mid_t = SigmaTType::Zero();
      SigmaRType _sigma_mid_r = SigmaRType::Zero();
      ;

      SigmaTType _sigma_high_t = SigmaTType::Zero();
      SigmaRType _sigma_high_r = SigmaRType::Zero();
      ;

      //! @brief selector thresholds
      const float _low_thresh;
      const float _mid_thresh;

      //! @brief jic
      bool _setup;

    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    };

  } /* namespace matchables */
} /* namespace g2o */
