#pragma once
#include <cstdlib>
#include <iostream>
#include <vector>

#include <g2o/config.h>
#include <g2o/stuff/filesys_tools.h>
#include <g2o/stuff/string_tools.h>

#include <g2o/apps/g2o_cli/dl_wrapper.h>
#include <g2o/apps/g2o_cli/g2o_common.h>

namespace g2o {
  namespace matchables {

    //! @brief library loader wrapper to load our custom type library
    //!        works only on unix - for now
    class DynamicLibraryLoader {
    public:
      DynamicLibraryLoader() {
      }
      virtual ~DynamicLibraryLoader() {
      }

      static void loadAllTypesLibs();

    protected:
      static DlWrapper _loader;

    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    };

  } // namespace matchables
} // namespace g2o
