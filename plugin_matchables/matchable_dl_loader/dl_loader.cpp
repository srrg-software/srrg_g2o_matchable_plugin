#include "dl_loader.h"
#include <dlfcn.h>

#define PATH_SEPARATOR ":"
static Dl_info dl_info;
static const std::string TYPES_PATTERN =
  std::string("*_types_*") + std::string("") + std::string(".so");

namespace g2o {
  namespace matchables {

    DlWrapper DynamicLibraryLoader::_loader;

    void DynamicLibraryLoader::loadAllTypesLibs() {
      std::string path = G2O_MATCHABLE_DEFAULT_LIB_DIR;
      if (dladdr(&dl_info, &dl_info) != 0) {
        path = getDirname(dl_info.dli_fname);
      }

      std::vector<std::string> paths = strSplit(path, PATH_SEPARATOR);
      int num_libraries              = -1;
      for (std::vector<std::string>::const_iterator it = paths.begin(); it != paths.end(); ++it) {
        if (it->size() > 0)
          num_libraries = _loader.openLibraries(*it, TYPES_PATTERN);
      }
      std::cerr << "[DynamicLibraryLoader::loadAllTypesLibs] loaded " << num_libraries
                << " types libraries" << std::endl
                << std::endl;
    }

  } // namespace matchables
} // namespace g2o
